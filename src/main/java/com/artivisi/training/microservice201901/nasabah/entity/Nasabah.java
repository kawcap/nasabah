package com.artivisi.training.microservice201901.nasabah.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity @Data
public class Nasabah {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty @Size(min = 3, max = 100)
    private String nomor;

    @NotEmpty @Size(min = 3, max = 255)
    private String namaDepan;

    @NotEmpty @Size(min = 3, max = 255)
    private String namaBelakang;

    @NotEmpty @Email
    private String email;

    @NotEmpty @Size(min = 5, max = 255)
    private String noHp;

}
