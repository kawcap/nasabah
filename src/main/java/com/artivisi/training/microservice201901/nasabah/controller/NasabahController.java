package com.artivisi.training.microservice201901.nasabah.controller;

import com.artivisi.training.microservice201901.nasabah.dao.NasabahDao;
import com.artivisi.training.microservice201901.nasabah.entity.Nasabah;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/nasabah")
public class NasabahController {

    @Autowired private NasabahDao nasabahDao;

    @GetMapping("/semua")
    public Iterable<Nasabah> dataNasabahTanpaPaging() {
        return nasabahDao.findAll();
    }

    @GetMapping("/")
    public Page<Nasabah> dataSemuaNasabah(Pageable page){
        return nasabahDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Nasabah cariById(@PathVariable(name = "id") Nasabah nasabah){
        return nasabah;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void insert(@RequestBody @Valid Nasabah nasabah){
        nasabahDao.save(nasabah);
    }
}
